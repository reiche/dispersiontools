import sys
import datetime
import os.path
import time
import subprocess
from os import walk
import numpy as np
import scipy.optimize as sop
import sys
import re
import webbrowser
import io
import string
import itertools


from epics import PV,caget

from PyQt5 import QtWidgets,QtCore,QtGui
from PyQt5.uic import loadUiType

import matplotlib.pyplot as plt

from matplotlib.figure import Figure
import matplotlib.patches as patches

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)


sys.path.append('/sf/bd/applications/pyREALTA/latest')
import pyREALTA
import DispersionModel

class DispersionTools(pyREALTA.pyREALTA): # pyREALTA has most of the default GUI interaction
    
    def init_dispersiontools(self):
        self.core.scan.sigterm.connect(self.analyse)
        self.model=DispersionModel.DispersionModel()
        self.beamline=0  # default Aramis
        self.AramisOrder = ['SARCL','SARMA','SARUN','SARBD']
        self.AthosOrder = ['S20','SATSY','SATCL','SATDI']
        self.UIModelSimple.clicked.connect(self.calcSimpleModel)
        self.UIModelEnergy.clicked.connect(self.calcEnergyDeviation)
        self.UIModelDisp1.clicked.connect(self.calcDispersion1)
        self.UIModelDisp2.clicked.connect(self.calcDispersion2)
        self.UIScaleMagnets.clicked.connect(self.setMagnets)
        self.UIStart.clicked.connect(self.switchToLivePlot)
        self.ModelType=-1

    #-----------------------------------------
    # routine to scale magnets


    def getBeamline(self):
        text=str(self.UIMacro.currentText())
        dest = 0  # Aramis
        if 'Athos' in text:
            dest = 1
        return dest


    def setMagnets(self):
        if self.ModelType < 0:
            return
        mag = {}
        val = 0.
        for i in range(self.UIModelValues.rowCount()):
            channel = str(self.UIModelValues.item(i,0).text())
            val = float(self.UIModelValues.item(i,1).text())
            mag[channel]=val
        # generate PVs
        if self.ModelType == 0:
            mag = self.model.getMagnets()
            for key in mag.keys():
                mag[key]=val
        if self.ModelType == 1:
            self.changeMagnets(mag)
        else:
            self.scaleMagnets(mag)

    def changeMagnets(self,mag):
        PVs=[PV(key+':K1L-SET') for key in sorted(mag.keys())]        
        valset = [mag[key] for key in sorted(mag.keys())]
        valcur = [pv.get() for pv in PVs]
        for i in range(len(valcur)):
           self.core.log.insertPlainText('Setting %s from %f to %f\n' % (PVs[i].pvname,valcur[i],valcur[i]-valset[i]))
           if 'MQSK300' in PVs[i].pvname:
               PVs[i].put(valcur[i]-valset[i])  # MQSK300 needs negative sign
           else:
               PVs[i].put(valcur[i]+valset[i])


 

    def scaleMagnets(self,mag):
        PVs=[PV(key+':I-SET') for key in sorted(mag.keys())]
        valset = [1+mag[key]*0.01 for key in sorted(mag.keys())]
        valcur = [pv.get() for pv in PVs]
        for i in range(len(valcur)):
           self.core.log.insertPlainText('Scaling %s from %f to %f\n' % (PVs[i].pvname,valcur[i],valcur[i]/valset[i]))
           PVs[i].put(valcur[i]*valset[i])

    def checkForDataset(self):
        if len(self.core.scan.derivedata.keys()) <=0:
            self.core.log.insertPlainText('*** Error: No valid dispersion data in memory')
            self.core.log.insertPlainText('           Please measure or load data first')
            return False
        return True



    #-------------------------------------------
    # routines to model the observation
    def calcSimpleModel(self):
        if not self.checkForDataset():
            return



    def calcEnergyDeviation(self):
        if not self.checkForDataset():
            return
        res = self.model.fitScaledMagnetStrength(self.core.scan.derivedata,self.core.scan.snapval)
        if not res:
            return
        self.ModelType=0
        self.UILog.appendPlainText('Best Fit for Energy Mismatch of %7.4f\n' % res['Scale'])
        scl = res['Scale']-1.
        self.updateTable([['All','%6.3f' % (scl*100.)]])
        self.plotModel(res['Meas'],res['Model'],res['Labels'],['DX'])


    def calcDispersion1(self):
        if not self.checkForDataset():
            return
        res = self.model.fitDispersion1(self.core.scan.derivedata,self.core.scan.snapval)
        if not res:
            return
        self.ModelType=1
        self.updateTable([[key,'%6.3f' % res['Scale'][key]] for key in res['Scale'].keys()])
        self.plotModel(res['Meas'],res['Model'],res['Labels'],['DX','DY'])
 
    def calcDispersion2(self):
        if not self.checkForDataset():
            return
        res = self.model.fitDispersion2(self.core.scan.derivedata,self.core.scan.snapval)
        if not res:
            return
        self.ModelType=2
        self.updateTable([[key,'%6.3f' % res['Scale'][key]] for key in res['Scale'].keys()])
        self.plotModel(res['Meas'],res['Model'],res['Labels'],['DDX','DDY'])


    def updateTable(self,entries):
        vlabel='Scale (%)'
        if self.ModelType==1:
            vlabel = 'Strength (k1L)'
        self.UIModelValues.clear()
        self.UIModelValues.setColumnCount(2)
        self.UIModelValues.setHorizontalHeaderItem(0, QtWidgets.QTableWidgetItem('Magnets'))
        self.UIModelValues.setHorizontalHeaderItem(1, QtWidgets.QTableWidgetItem(vlabel))
        self.UIModelValues.setRowCount(len(entries))
        for i,row in enumerate(entries):
             self.UIModelValues.setItem(i, 0, QtWidgets.QTableWidgetItem(row[0]))
             self.UIModelValues.setItem(i, 1, QtWidgets.QTableWidgetItem(row[1]))
        self.UIModelValues.resizeColumnsToContents()
        self.UIModelValues.verticalHeader().hide()

    def plotModel(self,ymeas,ymod,label,fields):
        lab={'DX':r'$1^{st}$ Order Dispersion $\eta_x$ (m)',
             'DY':r'$1^{st}$ Order Dispersion $\eta_y$ (m)',
             'DDX':r'$2^{nd}$ Order Dispersion $\eta^2_x$ (m)',
             'DDY':r'$2^{st}$ Order Dispersion $\eta^2_y$ (m)'}

        if (len(fields)<1) or (len(fields)>2):
            return
        single = len(fields)==1
        xbpm = np.array(range(0,len(label)))
        stride = 3
        # plot results
        if  single:
            self.axes.set(visible=True)        
            self.axest.set(visible=False)
            self.axesb.set(visible=False)
            self.axes.clear()
            self.axes.plot(xbpm,ymeas[fields[0]],'o--',label='Measurement')
            self.axes.plot(xbpm,ymod[fields[0]],label='Model')
            self.axes.legend()
            self.axes.set_ylabel(lab[fields[0]])
            self.axes.get_xaxis().set_ticks(xbpm[0::stride])
            self.axes.get_xaxis().set_ticklabels([lab.replace('-','-\n') for lab in label[0::stride]])

        else: 
            self.axes.set(visible=False)        
            self.axest.set(visible=True)
            self.axesb.set(visible=True)
            self.axest.clear()
            self.axesb.clear()
            self.axest.plot(xbpm,ymeas[fields[0]],'o--',label='Measurement')
            self.axest.plot(xbpm,ymod[fields[0]],label='Model')
            self.axest.legend()
            self.axest.set_ylabel(lab[fields[0]])

            self.axesb.plot(xbpm,ymeas[fields[1]],'o--',label='Measurement')
            self.axesb.plot(xbpm,ymod[fields[1]],label='Model')
            self.axesb.legend()
            self.axesb.set_ylabel(lab[fields[1]])
            self.axesb.get_xaxis().set_ticks(xbpm[0::stride])
            self.axesb.get_xaxis().set_ticklabels([lab.replace('-','-\n') for lab in label[0::stride]])
        self.canvas.draw()
            


    #-------------------------------------------------------------------------------
    # routines to calculate the dispersion from the measurement and plots them.
    def analyse(self,status):
        if status < 0:
            return
        results=self.calcDispersion()
        self.ModelType=-1
        if results:
            self.core.scan.derivedata = results  # save the derived data into the file
            self.plotDispersion(results)       

    def addenergy(self,energy,rf):
        for ele in rf:
            rfloc=np.array(self.core.scan.data[ele+'-RMSM:SM-GET'])
            rfon=np.array(rfloc==9)*1.0
            rfphase=np.array(self.core.scan.data[ele+'-RSYS:SET-BEAM-PHASE'])
            rfvolta=np.array(self.core.scan.data[ele+'-RSYS:SET-ACC-VOLT'])
            de=rfon*rfvolta*np.sin(rfphase*np.pi/180.)
            energy=energy+de
        return energy

    def getRelativeEnergy(self):
        self.rfin=[]  
        for i in range(0,4):
            self.rfin.append('SINSB%2.2d' % (i+1))
        for i in range(0,1):
            self.rfin.append('SINXB%2.2d' % (i+1))
        self.rfl1=[]
        for i in range(0,9):
            self.rfl1.append('S10CB%2.2d' % (i+1))
        self.rfl2=[]
        for i in range(0,4):
            self.rfl2.append('S20CB%2.2d' % (i+1))
        self.rfl3=[]
        for i in range(0,13):
            self.rfl3.append('S30CB%2.2d' % (i+1))        

        energy=7.1
        energy=self.addenergy(energy,self.rfin)
        energy=self.addenergy(energy,self.rfl1)
        energy=self.addenergy(energy,self.rfl2)
        if self.beamline == 0:   # aramis beamline
            energy=self.addenergy(energy,self.rfl3)

        E0=np.mean(energy)
        x=np.array((energy-E0)/E0)
        return x,energy

    def plotDispersion(self,results):
        order = self.AramisOrder
        if self.beamline == 1:
            order = self.AthosOrder
        x=[]
        y=[]
        sigx=[]
        sigy=[]
        x2=[]
        y2=[]
        sigx2=[]
        sigy2=[]
        label=[]
        for sec in order:
            fele = [ key for key in results.keys() if sec in key]
            for key in sorted(fele):
                label.append(key)
                x.append(results[key]['DX'])
                y.append(results[key]['DY'])
                x2.append(results[key]['DDX'])
                y2.append(results[key]['DDY'])                    
                sigx.append(results[key]['DX_ERR'])
                sigy.append(results[key]['DY_ERR'])
                sigx2.append(results[key]['DDX_ERR'])
                sigy2.append(results[key]['DDY_ERR'])
        nbpm = len(results)
        xbpm = np.linspace(1,nbpm,num=nbpm)
        stride=5
        self.axes.set(visible=False)        
        self.axest.set(visible=True)
        self.axesb.set(visible=True)
        self.axest.clear()
        self.axesb.clear()
        self.axest.errorbar(xbpm,x,yerr=sigx,label=r'$\eta_x$')
        self.axest.errorbar(xbpm,y,yerr=sigy,label=r'$\eta_y$')
        self.axest.set_ylabel(r'$1^{st}$ Order Dispersion $\eta$ (m)')
        self.axest.legend()
        self.axest.get_xaxis().set_visible(False)
        self.axesb.errorbar(xbpm,x2,yerr=sigx2,label=r'$\eta_x^2$')
        self.axesb.errorbar(xbpm,y2,yerr=sigy2,label=r'$\eta_y^2$')
        self.axesb.set_ylabel(r'$2^{nd}$ Order Dispersion $\eta^2$ (m)')
        self.axesb.get_xaxis().set_ticks(xbpm[0::stride])
        self.axesb.get_xaxis().set_ticklabels([lab.replace('-','-\n') for lab in label[0::stride]])
        self.axesb.legend()
        self.canvas.draw()
        self.core.storePlot()

    def switchToLivePlot(self):
        self.axes.set(visible=True)        
        self.axest.set(visible=False)
        self.axesb.set(visible=False)


    def calcDispersion(self):  

        results={}  # empty the result

        if len(self.core.scan.actuator.actuators) < 1:
            return None
            
        for key in self.core.scan.actuator.actuators.keys():
            xname=key
        if 'val' in self.core.scan.actuator.actuators[xname].keys():
            x = self.core.scan.actuator.actuators[xname]['val']
        else:
            return None
        
        # check BPM channels:

        self.BPMS = []
        for key in self.core.scan.data.keys():
            if 'SAR' in key:
                self.beamline = 0
            if 'SAT' in key:
                self.beamline = 1
            if 'DBPM' in key:
                self.BPMS.append(key)

        x,energy = self.getRelativeEnergy()
        x=np.reshape(x,(x.size))
        
        for bpm in self.BPMS:
            bpmroot=bpm[0:15]
            if not bpmroot in results.keys():
                results[bpmroot]={}
            y = self.core.scan.data[bpm]*1e-3
            y=np.reshape(y,(y.size))
            res,cov=np.polyfit(x,y,2,full=False,cov=True) 
            tag = 'X'
            if ':Y' in bpm:
                tag = 'Y'
            results[bpmroot]['D%s' % tag]=res[1]
            results[bpmroot]['DD%s' % tag]=res[0]
            results[bpmroot]['D%s_ERR'% tag]=np.sqrt(cov[1,1])
            results[bpmroot]['DD%s_ERR' % tag]=np.sqrt(cov[0,0])
        return results
 


#--------------------------
# wrapper for the GUI

if __name__  == '__main__':
    Ui_DispersionToolsGUI, QMainWindow = loadUiType('DispersionTools.ui')
    class DispersionToolsWrap(QMainWindow,Ui_DispersionToolsGUI,DispersionTools):
        def __init__(self,):
            super(DispersionToolsWrap,self).__init__()
            self.setupUi(self)
            self.initmpl()
            self.init_general(program='DispersionTools',version='v2.0.0')
            self.init_dispersiontools()

        def initmpl(self):
            self.fig=Figure()
            self.axes=self.fig.add_subplot(111)
            self.axest=self.fig.add_subplot(211)
            self.axesb=self.fig.add_subplot(212)
            self.axest.set(visible=False)
            self.axesb.set(visible=False)
            self.canvas = FigureCanvas(self.fig)
            self.mplvl.addWidget(self.canvas)
            self.canvas.draw()
            self.toolbar=NavigationToolbar(self.canvas,self.mplwindow, coordinates=True)
            self.mplvl.addWidget(self.toolbar)


# --------------------------------
# Main routine

if __name__ == '__main__':
    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("plastique"))
    app=QtWidgets.QApplication(sys.argv)
    main=DispersionToolsWrap()
    macros = ['DispersionToolAthos.json','DispersionToolAramis.json']
    for macro in macros:
        main.addMacro(macro)
    main.show()
    sys.exit(app.exec_())

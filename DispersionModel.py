import re
import sys
import numpy as np

sys.path.append('/sf/bd/applications/OnlineModel/current')
import OMFacility
import OMMadxLat 

class DispersionModel:
    def __init__(self):  
        self.Facility = OMFacility.Facility(1)  # initialize the lattice
        self.Facility.forceEnergyAt('SINLH01.DBAM010', 140e6)    
        self.Madx = OMMadxLat.OMMadXLattice()

    def getMagnets(self):
        regx = re.compile('SAR.*M[QS].[AKX]')  # filter out the permanent magnets
        mag = {}
        for key in self.Facility.ElementDB.keys():
            if regx.match(key):
                mag[key.replace('.','-')]=1.
        return mag

    def writeLattice(self):
        sec = self.Facility.getSection('SARBD01')
        path = sec.mapping
        line = self.Facility.BeamPath(path)
        line.setRange('SARCL01','SARBD01')
        self.Madx.writeHeader(30,30,0,0,5000) # betax, betay, alphax, alphy, energy (MeV)
        # look into Optics/MadXTracker.py for a better way switching between branches
        line.writeLattice(self.Madx, self.Facility)  # write lattice to madx

    def getModelValues(self,dispersion,snap,branch = 0):
        fitval = {}
        regx = re.compile('SARUN.*BPM')
        for key in dispersion:
            if regx.match(key):
                fitval[key] = dispersion[key]

        fitmag = {}
        regx = re.compile('SAR.*-M[QS].*:K.*-SET')
        for key in snap.keys():
            if regx.match(key):
                mag = key.split(':')[0]
                ele=mag.replace('-','.')
                if ele in self.Facility.ElementDB.keys():
                    val = snap[key]['val']
                    if not isinstance(snap[key]['val'],float):
                        val = snap[key]['val'][0] 
#                    print(mag,key,snap[key]['val'],ele,self.Facility.ElementDB[ele].Length)
                    fitmag[mag] = val/self.Facility.ElementDB[ele].Length
        return fitval,fitmag


    def getModelPrediciton(self,dispersion,snap,branch = 0):
        print('Model Prediciton')

    def fitDispersion2(self,dispersion,snap, branch = 0):
        self.writeLattice()
        fitval,fitmag = self.getModelValues(dispersion,snap)
        
        # update the lattice
        sext = []
        for key in fitmag.keys():
            if 'MSEX' in key:
                sext.append('%s.k2' % (key.replace('-','.')))
                self.Madx.write('%s.k2 := %f * scale;\n' % (key.replace('-','.'),fitmag[key]))
            else:
                self.Madx.write('%s.k1 := %f;\n' % (key.replace('-','.'),fitmag[key]))
        self.Madx.write('scale = 1;\n')
        self.Madx.write('\nuse, sequence=swissfel;\n')
        self.Madx.write('match,chrom,sequence=swissfel,range=#s/#e,betx=betax0,bety=betay0,alfx=alphax0,alfy=alphay0;\n')
        self.Madx.write('vary,name=scale,step=0.0001;\n')        
        for key in fitval.keys():
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DDX = 0;\n' % (key.replace('-','.')))
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DDY = 0;\n' % (key.replace('-','.')))
        self.Madx.write('lmdif,calls=300,tolerance=1e-21;\n')
        self.Madx.write('endmatch;\n')  
        self.Madx.write('scl1 = scale;\n')

        self.Madx.write('match,chrom,sequence=swissfel,range=#s/#e,betx=betax0,bety=betay0,alfx=alphax0,alfy=alphay0;\n')
        self.Madx.write('vary,name=scale,step=0.0001;\n')        
        for key in fitval.keys():
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DDX = %f;\n' % (key.replace('-','.'),fitval[key]['DDX']))
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DDY = %f;\n' % (key.replace('-','.'),fitval[key]['DDY']))
        self.Madx.write('lmdif,calls=300,tolerance=1e-21;\n')
        self.Madx.write('endmatch;\n')  
        self.Madx.write('scl2 = scale;\n')
        self.Madx.write('select, flag = save, clear;\n')
        self.Madx.write('select,flag = save, class = variable, pattern="scl";\n')
        self.Madx.write('save, file = "MatchVal.dat";\n')
        self.Madx.writeFooter()

        localdir=self.Madx.track(True)
        res = self.Madx.parseTwissFile(localdir)
        if len(res) <1:
            return None
        rval={'DDX':[],'DDY':[]}
        mval={'DDX':[],'DDY':[]}
        label=[]
        for i,line in enumerate(res['NAME']):
            if 'MARK' in line and 'DBPM' in line and 'SARUN' in line:
                key = line.split('.MARK')[0].replace('.','-')
                label.append(key)
                rval['DDX'].append(res['DDX'][i])
                rval['DDY'].append(res['DDY'][i])
                mval['DDX'].append(fitval[key]['DDX']) 
                mval['DDY'].append(fitval[key]['DDY']) 

        try:
            fin=open('%s/MatchVal.dat' % localdir, 'r')
        except FileNotFoundError:
            print('Match File not found')
            return None
        val = None
        scl1 = 1.
        scl2 = 1.
        for line in fin:
            ele = line.split('=')
            val = float(ele[1][0:-2])
            if 'scl1' in line:
                scl1 = val
            if 'scl2' in line:
                scl2 = val
        fin.close()
        if val == None:
            return None
        res={}
        for sex in sext:
            res[sex[0:15].replace('.','-')]=(scl2/scl1-1)*100.
        retval ={'Meas':mval,'Model':rval,'Labels':label,'Scale':res}
        return retval

    def fitDispersion1(self,dispersion,snap, branch = 0):
        self.writeLattice()
        fitval,fitmag = self.getModelValues(dispersion,snap)
        
#        cors = ['SARCL02.MQ%s.k1' %s for s in ['UA150','SK160','SK300','SK420','UA430']]
        cors = ['SARCL02.MQ%s.k1' %s for s in ['UA150','SK300','SK160','UA430']] # - the best so far
#        cors = ['SARCL02.MQ%s.k1' %s for s in ['UA150','SK300','UA430']]
        # update the lattice
        for key in fitmag.keys():
            if 'MSEX' in key:
                self.Madx.write('%s.k2 := %f;\n' % (key.replace('-','.'),fitmag[key]))
            else:
                self.Madx.write('%s.k1 := %f;\n' % (key.replace('-','.'),fitmag[key]))
        self.Madx.write('\nuse, sequence=swissfel;\n')

        # first save the values for ideal settings
        self.Madx.write('match,sequence=swissfel,range=#s/#e,betx=betax0,bety=betay0,alfx=alphax0,alfy=alphay0;\n')
        for cor in cors:
            self.Madx.write('vary,name=%s,step=0.0001;\n' % cor)                       
        for key in fitval.keys():
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DX = %f;\n' % (key.replace('-','.'),0))
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DY = %f;\n' % (key.replace('-','.'),0))
        self.Madx.write('lmdif,calls=300,tolerance=1e-21;\n')
        for i,cor in enumerate(cors):
            self.Madx.write('var%d = %s;\n' % (i,cor))
        self.Madx.write('endmatch;\n')   

        self.Madx.write('match,sequence=swissfel,range=#s/#e,betx=betax0,bety=betay0,alfx=alphax0,alfy=alphay0;\n')
        for cor in cors:
            self.Madx.write('vary,name=%s,step=0.0001;\n' % cor)                       
        for key in fitval.keys():
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DX = %f;\n' % (key.replace('-','.'),fitval[key]['DX']))
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DY = %f;\n' % (key.replace('-','.'),fitval[key]['DY']))
        self.Madx.write('lmdif,calls=300,tolerance=1e-21;\n')
        for i,cor in enumerate(cors):
            self.Madx.write('%s.match = var%d-%s;\n' % (cor,i,cor))
        self.Madx.write('endmatch;\n')   




        self.Madx.write('select, flag = save, clear;\n')
        self.Madx.write('select,flag = save, class = variable, pattern="match";\n')
        self.Madx.write('save, file = "MatchVal.dat";\n')
        self.Madx.writeFooter()

        localdir=self.Madx.track(True)
        res = self.Madx.parseTwissFile(localdir)
        if len(res) <1:
            return None
        rval={'DX':[],'DY':[]}
        mval={'DX':[],'DY':[]}
        label=[]
        for i,line in enumerate(res['NAME']):
            if 'MARK' in line and 'DBPM' in line and 'SARUN' in line:
                key = line.split('.MARK')[0].replace('.','-')
                label.append(key)
                rval['DX'].append(res['DX'][i])
                rval['DY'].append(res['DY'][i])
                mval['DX'].append(fitval[key]['DX']) 
                mval['DY'].append(fitval[key]['DY']) 

        try:
            fin=open('%s/MatchVal.dat' % localdir, 'r')
        except FileNotFoundError:
            print('Match File not found')
            return None
        res={}
        for line in fin:
            print(line)
            ele = line.split(' ')
            name = ele[0].upper()[:15]
            val = ele[-1].split(';')[0]
            if name+'.k1' in cors:
                if name in self.Facility.ElementDB.keys():
                    length = self.Facility.ElementDB[name].Length
                    print(name,float(val),length)
                    res[name.replace('.','-')]=float(val)*length
        print('corrector magnets k1L')
        print(res)
        fin.close()

        retval ={'Meas':mval,'Model':rval,'Labels':label,'Scale':res}
        return retval

    def fitScaledMagnetStrength(self,dispersion,snap, branch = 0):
        self.writeLattice()
        fitval,fitmag = self.getModelValues(dispersion,snap)
 
        # matching specific text
        self.Madx.write('scale := 1.0;\n')
        for key in fitmag.keys():
            if 'MSEX' in key:
                self.Madx.write('%s.k2 := %f*scale;\n' % (key.replace('-','.'),fitmag[key]))
            else:
                self.Madx.write('%s.k1 := %f*scale;\n' % (key.replace('-','.'),fitmag[key]))

        self.Madx.write('\nuse, sequence=swissfel;\n')
        self.Madx.write('match,sequence=swissfel,range=#s/#e,betx=betax0,bety=betay0,alfx=alphax0,alfy=alphay0;\n')
        self.Madx.write('vary,name=scale,step=0.0001;\n')        
        for key in fitval.keys():
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DX = 0;\n' % (key.replace('-','.')))
        self.Madx.write('lmdif,calls=300,tolerance=1e-21;\n')
        self.Madx.write('endmatch;\n')   
        self.Madx.write('scl1=scale;\n')
        
        self.Madx.write('match,sequence=swissfel,range=#s/#e,betx=betax0,bety=betay0,alfx=alphax0,alfy=alphay0;\n')
        self.Madx.write('vary,name=scale,step=0.0001;\n')        
        for key in fitval.keys():
            self.Madx.write('CONSTRAINT,SEQUENCE=swissFEL,range=%s.Mark,DX = %f;\n' % (key.replace('-','.'),fitval[key]['DX']))
        self.Madx.write('lmdif,calls=300,tolerance=1e-21;\n')
        self.Madx.write('endmatch;\n')   
        self.Madx.write('scl2=scale;\n')
        self.Madx.write('select, flag = save, clear;\n')
        self.Madx.write('select,flag = save, class = variable, pattern="scl";\n')
        self.Madx.write('save, file = "MatchVal.dat";\n')
        self.Madx.writeFooter()

        localdir=self.Madx.track(True)
        res = self.Madx.parseTwissFile(localdir)
        if len(res)<1:
            return None
        rval={'DX':[]}
        mval={'DX':[]}
        label=[]
        for i,line in enumerate(res['NAME']):
            if 'MARK' in line and 'DBPM' in line and 'SARUN' in line:
                key = line.split('.MARK')[0].replace('.','-')
                label.append(key)
                rval['DX'].append(res['DX'][i])
                mval['DX'].append(fitval[key]['DX']) 
        try:
            fin=open('%s/MatchVal.dat' % localdir, 'r')
        except FileNotFoundError:
            print('Match File not found')
            return None

        val = None
        scl1 = 1.
        scl2 = 1.
        for line in fin:
            ele = line.split('=')
            val = float(ele[1][0:-2])
            if 'scl1' in line:
                scl1 = val
            if 'scl2' in line:
                scl2 = val
                
        fin.close()
        if val == None:
            return None
                
        retval ={'Meas':mval,'Model':rval,'Labels':label,'Scale':scl2/scl1}
        return retval

        
